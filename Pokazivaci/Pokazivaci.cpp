﻿#include <stdio.h>

void main() {

	//Inicijalizacija varijable a
	int a = 5;
	printf("&a=%d, a=%d", &a, a);
	
	//Spremi adresu varijable 'a' u pokazivač 'b'
	int *b = &a;
	printf("\n&b=%d, b=%d", &b, b);

	//Pokazivač b
	printf("\nb=%d, *b=%d", b, *b);

	getchar();
}